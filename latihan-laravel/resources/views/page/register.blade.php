<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sign Up - SanberBook</title>
</head>

<body>
        <h1>Buat Account Baru!</h1>
        <h2>Sign Up Form</h2>
      <form action="/daftar" method="POST" >
        @csrf
          <label>First Name:</label> <br>
          <input type="text" name="first"> <br><br>
          <label>Last Name</label> <br>
          <input type="text" name="last">
        <p>Gender:<br>
            <input type="radio" name="gender">Male <br>
            <input type="radio" name="gender">Female <br>
            <input type="radio" name="gender">Other
        </p>
            <p>Nationality:
            <select name="Negara">
            <option value="Indonesia">Indonesia</option>
            <option value="Malaysia">Malaysia</option>
            <option value="Singapura">Singapura</option>
            <option value="Amerika">Amerika</option>
            </select>
            </p>
            <p>Language Spoken:<br>
            <input type="checkbox">Bahasa Indonesia <br>
            <input type="checkbox">English <br>
            <input type="checkbox">Other
            </p>
            <p>Bio:<br>
            <textarea name="alamat" id="" cols="30" rows="10"></textarea>
            </p>
            <input type="submit" value="Sign Up">

      </form>

</body>

</html>