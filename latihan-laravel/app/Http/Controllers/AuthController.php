<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function daftar(){
        return view('page.register');
    }
    public function submit(Request $request){
        $first_name = $request['first'];
        $last_name = $request['last'];
        return view('page.welcome', compact('first_name', 'last_name'));
    }
}
